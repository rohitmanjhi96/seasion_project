import React from 'react';
import ReactDOM from 'react-dom';
import SeasionDisplay from './SeasionDisplay';
import Spinner from './Spinner';

class App extends React.Component {
 
    state = {lat: null, error:""}
 
    componentDidMount() {                     
        window.navigator.geolocation.getCurrentPosition(
            position => this.setState({lat: position.coords.latitude}),
            err => this.setState({error: err.message})
            
        )
    }

    renderContent() {
        if (this.state.error && !this.state.lat){
            return <div>Error: {this.state.error} </div>
        }

        if (!this.state.error && this.state.lat){
            return <SeasionDisplay lat={this.state.lat} />
           
        }

        return <Spinner message= "Please acept Loading requests..." />;
        
   
    

    }
    render() {
        return (
            <div className="border red">
             {this.renderContent()}
            </div>
        )  
    }    
}

ReactDOM.render(
    <App />, 
    document.querySelector('#root')
); 